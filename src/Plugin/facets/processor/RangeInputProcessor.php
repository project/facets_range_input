<?php

namespace Drupal\facets_range_input\Plugin\facets\processor;

use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\PostQueryProcessorInterface;
use Drupal\facets\Processor\PreQueryProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;
use Drupal\facets\Result\Result;

/**
 * Provides a processor that adds all range values between a min and max range.
 *
 * @FacetsProcessor(
 *   id = "range_input",
 *   label = @Translation("Range Input"),
 *   description = @Translation("Add range results for all the steps between min and max range."),
 *   stages = {
 *     "pre_query" = 60,
 *     "post_query" = 60,
 *     "build" = 20
 *   }
 * )
 */
class RangeInputProcessor extends ProcessorPluginBase implements PreQueryProcessorInterface, PostQueryProcessorInterface, BuildProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function preQuery(FacetInterface $facet) {
    $active_items = $facet->getActiveItems();
    array_walk($active_items, function (&$item) {
      if (preg_match('/\(min:((?:-)?[\d\.]+),max:((?:-)?[\d\.]+)\)/i', $item, $matches)) {
        $item = [$matches[1], $matches[2]];
      }
      else {
        $item = NULL;
      }
    });
    $facet->setActiveItems($active_items);
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results): array {
    /** @var \Drupal\facets\Plugin\facets\processor\UrlProcessorHandler $url_processor_handler */
    $url_processor_handler = $facet->getProcessors()['url_processor_handler'];
    $url_processor = $url_processor_handler->getProcessor();
    $active_filters = $url_processor->getActiveFilters();

    if (isset($active_filters[''])) {
      unset($active_filters['']);
    }

    /** @var \Drupal\facets\Result\ResultInterface[] $results */
    foreach ($results as &$result) {
      $new_active_filters = $active_filters;
      unset($new_active_filters[$facet->id()]);
      // Add one generic query filter with the min and max placeholder.
      $new_active_filters[$facet->id()][] = '(min:__range_input_min__,max:__range_input_max__)';
      $url = \Drupal::service('facets.utility.url_generator')->getUrl($new_active_filters, FALSE);
      $result->setUrl($url);
    }
    return $results;

  }

  /**
   * {@inheritdoc}
   */
  public function postQuery(FacetInterface $facet) {
    $simple_results = [];

    // Generate all the "results" between min and max.
    foreach ($facet->getResults() as $result) {
      $simple_results['f_' . (float) $result->getRawValue()] = [
        'value' => (float) $result->getRawValue(),
        'count' => (int) $result->getCount(),
      ];
    }
    uasort($simple_results, function ($a, $b) {
      return (int) $a['value'] - $b['value'];
    });

    $min = reset($simple_results)['value'] ?? 0;
    $max = end($simple_results)['value'] ?? 0;

    // Creates an array of all results between min and max.
    $new_results = [];
    $state = !empty($facet->getActiveItems());
    for ($i = $min; $i <= $max; $i++) {
      $count = isset($simple_results['f_' . $i]) ? $simple_results['f_' . $i]['count'] : 0;
      $new_result = new Result($facet, (float) $i, (float) $i, $count);
      $new_result->setActiveState($state);
      $new_results[] = $new_result;
    }

    // Overwrite the current facet values with the generated results.
    $facet->setResults($new_results);
  }

}
