<?php

namespace Drupal\facets_range_input\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Renders the range input form to use for the facet widget.
 */
class RangeInputForm extends FormBase {

  /**
   * The facet.
   *
   * @var \Drupal\facets\FacetInterface
   */
  protected FacetInterface $facet;

  /**
   * The widget configuration.
   *
   * @var array
   */
  protected array $configuration;

  /**
   * Creates a new RangeInputForm instance.
   *
   * @param \Drupal\facets\FacetInterface $facet
   *   The facet.
   * @param array $configuration
   *   The facet widget configuration.
   */
  public function __construct(FacetInterface $facet, array $configuration = []) {
    $this->facet = $facet;
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'facets_range_input_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['#attributes']['class'] = [
      'js-facets-widget',
      'js-facets-' . $this->facet->getWidget()['type'],
    ];
    $form['#theme'] = 'facets_range_input_form';

    foreach (['minimum', 'maximum'] as $field_id) {
      $form[$field_id] = [
        '#type' => 'number',
        '#title' => $this->configuration[$field_id . '_title'],
        '#placeholder' => $this->configuration[$field_id . '_placeholder'],
        '#field_suffix' => '<div id="' . $this->facet->id() . '_' . $field_id . '_validation"></div>',
        '#maxlength' => 10,
        '#size' => 10,
        '#weight' => 0,
        '#attributes' => [
          'id' => $this->facet->id() . '_' . $field_id,
          'facet-range-input-input-id' => $field_id,
          'facet-range-input-input-title' => $this->configuration[$field_id . '_title'],
          'facet-range-input-facet-id' => $this->facet->id(),
        ],
        '#required' => TRUE,
      ];
    }
    $form['validationContainer'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => ['validationContainer'],
        'class' => [
          'facets-range-input-valid-range',
        ],
      ],
    ];

    $form['apply'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#ajax' => [
        'callback' => '::ajaxSubmit',
        'event' => 'click',
      ],

    ];

    $form['#cache'] = ['max-age' => 0];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): bool {
    // parent::validateForm($form, $form_state);.
    if (empty($form_state->getValue('minimum'))) {
      $min = 0;
    }
    else {
      $min = $form_state->getValue('minimum');
    }

    if (empty($form_state->getValue('maximum'))) {
      return FALSE;
    }
    else {
      $max = $form_state->getValue('maximum');
    }
    if ($min >= $max) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Ajax submit form logic.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AJAX response.
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();
    if ($this->validateForm($form, $form_state)) {
      $payload = [
        'minimum' => $form_state->getValue('minimum'),
        'maximum' => $form_state->getValue('maximum'),
        'facetId' => $this->facet->id(),
      ];
      $response->addCommand(new InvokeCommand(NULL, 'facetsRangeInputFilter', [$payload]));
      return $response;
    }
    else {
      $invalidRangeText = $this->t('Please enter a valid range');
      $response->addCommand(new HtmlCommand('#validationContainer', $invalidRangeText));
      return $response;
    }
  }

}
